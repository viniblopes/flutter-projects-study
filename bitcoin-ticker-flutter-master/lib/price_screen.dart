import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

import 'coin_data.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = 'USD';
  Map<String, String> cryptoCurrency = {};
  bool isWaiting = false;

  Column getCoinList() {
    List<Widget> coins = [];
    for (String crypto in cryptoList) {
      coins.add(
        CryptoCard(
            crypto: crypto,
            coinConverted: isWaiting ? '?' : cryptoCurrency[crypto],
            selectedCurrency: selectedCurrency),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.center,
      children: coins,
    );
  }

  DropdownButton<String> androidPicker() {
    List<DropdownMenuItem<String>> dropDownItems = [];
    for (String currency in currenciesList) {
      dropDownItems.add(
        DropdownMenuItem(
          child: Text(currency),
          value: currency,
        ),
      );
    }

    return DropdownButton(
      value: selectedCurrency,
      items: dropDownItems,
      onChanged: (value) {
        setState(
          () {
            selectedCurrency = value;
          },
        );
        getData();
      },
    );
  }

  CupertinoPicker iOSPicker() {
    List<Text> dropDownItems = [];
    for (String currency in currenciesList) {
      dropDownItems.add(
        Text(currency),
      );
    }

    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (selectedIndex) {
        setState(
          () {
            selectedCurrency = currenciesList[selectedIndex];
          },
        );
        getData();
      },
      children: dropDownItems,
    );
  }

  void getData() async {
    isWaiting = true;
    try {
      var coinData = await CoinData().getCoinData(selectedCurrency);
      isWaiting = false;
      updateUI(coinData);
    } catch (e) {
      print(e);
    }
  }

  void updateUI(dynamic coinData) {
    if (coinData == null) {}
    setState(
      () {
        cryptoCurrency = coinData;
      },
    );
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          getCoinList(),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isAndroid ? androidPicker() : iOSPicker(),
          ),
        ],
      ),
    );
  }
}

class CryptoCard extends StatelessWidget {
  const CryptoCard({
    Key key,
    @required this.crypto,
    @required this.coinConverted,
    @required this.selectedCurrency,
    @required this.prices,
  }) : super(key: key);

  final String crypto;
  final String coinConverted;
  final String selectedCurrency;
  final Map<String, String> prices;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
      child: Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 $crypto = $coinConverted $selectedCurrency',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
