class Question {
  String questionText;
  bool questionAnswer;

  Question(this.questionText, this.questionAnswer);

  bool checkAnswer(bool a) {
    return this.questionAnswer == a;
  }
}
