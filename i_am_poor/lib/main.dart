import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.teal[50],
          appBar: AppBar(
            backgroundColor: Colors.teal,
            title: Text('I am Poor'),
          ),
          body: Center(
            child: Image(
              image: AssetImage('images/img.jpg'),
            ),
          ),
        ),
      ),
    );
