import 'package:flutter/material.dart';
// import 'package:petrol_app/screens/login_screen.dart';
// import 'package:petrol_app/screens/registration_screen.dart';
// import 'package:petrol_app/screens/welcome_screen.dart';
// import 'constants.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      // case welcomeScreenRoute:
      //   return MaterialPageRoute(builder: (_) => WelcomeScreen());
      // case loginScreenRoute:
      //   return MaterialPageRoute(builder: (_) => LoginScreen());
      // case registrationScreenRoute:
      //   return MaterialPageRoute(builder: (_) => RegistrationScreen());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                  child: Text('No route defined for ${settings.name}'),
                ),
              ),
        );
    }
  }
}
